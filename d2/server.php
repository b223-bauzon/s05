<?php

// session_start() is also added in the server.php so it is also included in the current session.
session_start();

// echo $_POST['description'];
// We can check that we have able to access the value of our session variable.
// echo $_SESSION['greet'];

class TaskList{

	// Add task
	public function add($description){
		// Create an object variable that will contain the details about the task.
		$newTask = (object)[
			'description' => $description,
			// by default an added task is not yet finished, so it is set to false.
			'isFinished' => false
		];

		// If there is no added tasks yet.
		if($_SESSION['tasks'] === null){
			/*

	        	- Session variables are set with the PHP global variable: $_SESSION.
	            - "$_SESSION" is an associative array that contains all session variables. It is used to set and get session variable values.
				- A global variable $_SESSION['tasks'] will be created with an empty array.

   			 */
			
			$_SESSION['tasks'] = array();
		}

		// Then $newTask will be added in the $_SESSION['tasks'] variable.
		array_push($_SESSION['tasks'], $newTask);

	}

	// Update a Task
	// The update ttask will be needing three parameters:
		// $id for searching specific task.
		// $description and $isFinished

	public function update($id, $description, $isFinished){
		// $_SESSION['tasks'][$id] is equal to "tasks[$id]" it will retrieve a specific task based on the index number.
		$_SESSION['tasks'][$id]->description = $description;

		// if the value of the checkbox is not equal to null, the isFinished value will be set to true, otherwise false.
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true: false;
	}

	// Delete a task
	public function remove($id){
		// Syntax: array_splice(array, startDel, length, newArrElement)
    	// A task will be removed from the" $_SESSION['tasks']" base on its $id(index) and will only removed one task.
		array_splice($_SESSION['tasks'], $id, 1);
	}

	// Remove all the tasks
	public function clear(){
		// removes all of the data associated with the current session.
		session_destroy();
	}
}

// taskList is instantiated from the TaskList() class to have access with its method.
$taskList = new TaskList();

// This will handle the action sent by the user.
if($_POST['action'] === 'add'){
	$taskList->add($_POST['description']);
}
else if ($_POST['action'] === 'update'){
	$taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}
else if($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);
}
else if($_POST['action'] === 'clear'){
	$taskList->clear();
}	

// it will redirect us to the index file upon sending the request.
header('Location: ./index.php');
